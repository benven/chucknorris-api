import axios from 'axios';

function getJoke(n: number, setJokes: Function, setIsLoading: Function) {
  for (let i = 0; i < n; i++) {
    axios
      .get('https://api.chucknorris.io/jokes/random')
      .then((res) => {
        setIsLoading(true);
        setJokes((jokes: Object[]) => [...jokes, res.data]);
      })
      .finally(setIsLoading(false));
  }

  setJokes([]);
}

export default getJoke;
