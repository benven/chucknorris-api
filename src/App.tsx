import CardsContainer from './components/CardsContainer/CardsContainer';
import {useState} from 'react';
import PanelContainer from './components/PanelContainer/PanelContainer';
import './App.css';

function App() {
  const nbInitialCards: number = 5;

  const [jokes, setJokes] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  return (
    <div className="App">
      <h1 className="text-center mb-10 mt-10 text-5xl text-white">Chuck Norris API</h1>

      {!isLoading ? (
        <p className="text-center text-white mt-36">Loading...</p>
      ) : (
        ''
      )}
      <CardsContainer
        nbInitialCards={nbInitialCards}
        jokes={jokes}
        setJokes={setJokes}
        setIsLoading={setIsLoading}
      />
      <PanelContainer setJokes={setJokes} setIsLoading={setIsLoading} />
    </div>
  );
}

export default App;
