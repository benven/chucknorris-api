import {useState} from 'react';
import Button from '../Button/Button';
import Range from '../Range/Range';

function PanelContainer({...props}) {
  const initialRangedValue = 10;
  const [rangeValue, setRangeValue] = useState(initialRangedValue);

  return (
    <div className="fixed bottom-0 left-0 right-0 flex justify-around p-1 bg-indigo-900 border-t-4 border-indigo-600 PanelContainer h-14">
      <Button props={props} rangeValue={rangeValue} />
      <Range setRangeValue={setRangeValue} rangeValue={rangeValue} />
    </div>
  );
}

export default PanelContainer;
