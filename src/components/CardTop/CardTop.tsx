import ImageComponent from '../ImageComponent/ImageComponent';

function CardHeader() {
  return (
    <div className="flex justify-center cardHeader h-42">
      <ImageComponent />
    </div>
  );
}

export default CardHeader;
