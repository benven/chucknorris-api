import Description from '../Description/Description';
import Votation from '../Votation/Votation';

function CardBottom() {
  return (
    <div className="flex flex-col cardBottom lg:flex-row xl:flex-row 2xl:flex-row mt-12">
      <Description />
      <Votation />
    </div>
  );
}

export default CardBottom;
