import {useContext} from 'react';
import {JokeContext} from '../CardsContainer/CardsContainer';
import './Description.css';

function Description() {
  const joke: any = useContext(JokeContext);
  const description = joke.value;
  const iconPng = joke.icon_url;

  return (
    <div className="p-4 lg:w-3/5 md:w-full sm:w-full Description bg-blue-800 mr-2">
      <p className="italic text-justify text-white">{description} </p>

      <a
        className="font-bold text-white hover:underline hover:text-blue-600 opacity-40"
        target="_blank"
        href={joke.url}
      >
        <img
          className="mt-5 opacity-70 hover:animate-spin"
          src={iconPng}
          alt="an icon related to Chuck Norris."
        />
      </a>
    </div>
  );
}

export default Description;
