import getJoke from '../../api/getJoke';

function Button({rangeValue, ...props}) {
  const {setJokes, setIsLoading} = props.props;

  return (
    <button
      onClick={() => getJoke(rangeValue, setJokes, setIsLoading)}
      className="p-2 bg-blue-100 rounded-sm hover:animate-pulse hover:bg-indigo-400"
    >
      Generate {rangeValue} news jokes.
    </button>
  );
}

export default Button;
