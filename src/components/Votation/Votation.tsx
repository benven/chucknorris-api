import React, {useContext, useDebugValue, useState} from 'react';
import useCount from '../../hooks/useCount';

import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faArrowUp} from '@fortawesome/free-solid-svg-icons';
import {faArrowDown} from '@fortawesome/free-solid-svg-icons';

function Votation() {
  const initialVoteNumber = 5;
  const [count, increment, decrement, reset] = useCount(initialVoteNumber);

  function getColor(): string {
    if (count >= 15) {
      return '#4CAF50';
    } else if (count >= 12) {
      return '#8BC34A';
    } else if (count >= 9) {
      return '#CDDC39';
    } else if (count >= 6) {
      return '#FFEB3B';
    } else if (count >= 3) {
      return '#FFC107';
    } else if (count >= 0) {
      return '#FF9800';
    } else {
      return '#f44336';
    }
  }
  function getEmoji(): string {
    if (count >= 15) {
      return '😆';
    } else if (count >= 12) {
      return '😁';
    } else if (count >= 9) {
      return '😀';
    } else if (count >= 6) {
      return '🙂';
    } else if (count >= 3) {
      return '😕';
    } else if (count >= 0) {
      return '😟';
    } else {
      return '😩';
    }
  }
  return (
    <div className="relative p-4 rounded-xl bg-blue-800 lg:w-2/5 lg:h-full sm:w-full">
      <div className="flex justify-between Votation">
        <button
          className="text-2xl text-white hover:text-blue-500"
          onClick={increment}
        >
          <FontAwesomeIcon icon={faArrowUp} />
        </button>

        <span className="text-5xl">{getEmoji(count)}</span>

        <button
          className="text-2xl text-white hover:text-blue-500"
          onClick={decrement}
        >
          <FontAwesomeIcon icon={faArrowDown} />
        </button>
      </div>

      {count !== initialVoteNumber ? (
        <button
          className="absolute right-0 bottom-0 p-1  text-white rounded bg-slate-900 hover:bg-gray-800 rounded-tl-md"
          onClick={reset}
        >
          reset
        </button>
      ) : null}

      <p
        className="p-1 font-black text-center"
        style={{color: getColor(count)}}
      >
        {count}
      </p>
    </div>
  );
}

export default Votation;
