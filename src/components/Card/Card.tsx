import CardBottom from '../CardBottom/CardBottom';
import CardTop from '../CardTop/CardTop';

function Card() {
  return (
    <div className="w-11/12 p-5 m-1 bg-black border-2 rounded-lg shadow-2xl shadow-inner bg-opacity-10 xl:w-3/12 md:w-2/6 sm:w-full Card backdrop-filter backdrop-blur-sm">
      <CardTop />
      <CardBottom />
    </div>
  );
}

export default Card;
