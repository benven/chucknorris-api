import React from 'react';

interface Props {
  setRangeValue: Function;
  rangeValue: number;
}

function Range({setRangeValue, rangeValue}: Props) {
  function handleChange(e: React.ChangeEvent) {
    setRangeValue(e.target.value);
  }

  return (
    <div>
      <input
        onChange={handleChange}
        type="range"
        min="1"
        max="40"
        className="slider"
        id="myRange"
        step="1"
        value={rangeValue}
      ></input>
      <p className="text-white">{rangeValue} Jokes</p>
    </div>
  );
}

export default Range;
