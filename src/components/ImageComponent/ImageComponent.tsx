function ImageComponent() {
  const photos: Object[] = [
    {
      name: 'cn-1.png',
      alt: 'Chuck Norris holding a gun!',
    },
    {
      name: 'cn-2.png',
      alt: 'Chuck Norris doing karate!',
    },
    {
      name: 'cn-3.png',
      alt: 'Chuck Norris dressed in cow boy!',
    },
    {
      name: 'cn-4.png',
      alt: 'Chuck Norris Master kungfu.',
    },
    {
      name: 'cn-5.png',
      alt: 'Chuck Norris holding a gun!',
    },
    {
      name: 'cn-6.png',
      alt: 'Chuck Norris holding two milk-shakes',
    },
    {
      name: 'cn-7.png',
      alt: 'Chuck Norris dressed like a bad boy!',
    },
    {
      name: 'cn-8.png',
      alt: 'Chuck Norris dressed in cowboy.',
    },
    {
      name: 'cn-9.png',
      alt: 'Chuck Norris dressed with a black suit and a cowboy hat!',
    },
    {
      name: 'cn-10.png',
      alt: 'Chuck Norris dressed like a policeman!',
    },
    {
      name: 'cn-11.png',
      alt: 'Chuck Norris dressed in black sheriff uniform!',
    },
    {
      name: 'cn-12.png',
      alt: 'Chuck Norris thinking.',
    },
  ];

  const randomIndex = Math.floor(Math.random() * photos.length);
  const src = `./src/images/${photos[randomIndex].name}`;
  return (
    <img className="w-36 border-b-4" src={src} alt={photos[randomIndex].alt} />
  );
}

export default ImageComponent;
