import {useEffect, useState} from 'react';
import Card from '../Card/Card';
// import useGetJokes from '../../hooks/useGetJokes';
import React from 'react';
import getJoke from '../../api/getJoke.ts';

export const JokeContext = React.createContext(null);

interface Props {
  nbInitialCards: number;
  jokes: Object[];
  setJokes: Function;
  setIsLoading: Function;
}

function CardsContainer({
  nbInitialCards,
  jokes,
  setJokes,
  setIsLoading,
}: Props) {
  useEffect(() => {
    getJoke(nbInitialCards, setJokes, setIsLoading);
  }, []);

  const arrJokes = jokes.map((joke) => (
    <JokeContext.Provider value={joke} key={joke.id}>
      <Card key={joke.id} />
    </JokeContext.Provider>
  ));

  return (
    <div className="flex flex-wrap justify-center pb-24 mx-auto">
      {arrJokes}
    </div>
  );
}

export default CardsContainer;
